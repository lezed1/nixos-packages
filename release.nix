{ supportedSystems ? [ "x86_64-linux" ] }:

with (import <nixpkgs/pkgs/top-level/release-lib.nix> {
  inherit supportedSystems;
  nixpkgsArgs = { config = { allowUnfree = true; }; };
});

{
  cloudflare-warp = pkgs.lib.hydraJob (pkgs.callPackage ./cloudflare-warp { } );
}
