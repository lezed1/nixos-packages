let
nixpkgs = import <nixpkgs> { config.allowUnfree = true; };
callPackage = nixpkgs.pkgs.callPackage;
in
{
  cloudflare-warp = callPackage ./cloudflare-warp { };
  fireflies = callPackage ./fireflies { };
}
