{ stdenv, fetchFromGitHub, makeWrapper, xlibsWrapper, mesa, SDL, libXext, libX11 }:

stdenv.mkDerivation rec {
  name = "fireflies-2.07.1";

  src = fetchFromGitHub {
    owner = "lezed1";
    repo = "fireflies";
    rev = "fireflies-2.07.1";
    sha256 = "1qbyii16b9ncaa143b6ciw6i4bbmqs9p71c3nqkldgbc9b0406vq";
  };

  buildInputs = [ makeWrapper xlibsWrapper SDL libXext mesa libX11 ];

  installPhase = ''
    chmod +x src/fireflies && mkdir -p $out/bin && cp src/fireflies $out/bin/fireflies
  '';

  postFixup = ''
    patchelf --set-rpath \"${libX11.out}/lib:$(patchelf --print-rpath $out/bin/fireflies)\" $out/bin/fireflies
    wrapProgram $out/bin/fireflies --prefix LD_LIBRARY_PATH : $(patchelf --print-rpath $out/bin/fireflies | sed -e 's/^"//' -e 's/"$//')
  '';

  meta = {
    description = "A screensaver that simulates fireflies in swarms.";
    longDescription = ''
      Fireflies is a highly configurable screensave that simulates swarms of fireflies that chase moving targets to produce a beautiful result.
    '';
    homepage = "https://github.com/lezed1/fireflies";
    license = stdenv.lib.licenses.gpl2;
    maintainers = [ "Zander Bolgar <lezed1@gmail.com>" ];
    platforms = stdenv.lib.platforms.all;
  };
}
